# dotfiles

This is a collection of dotfiles that I use on my personal Debian 10 + XFCE
installation, with `i3` as window manager.

## Installation

To replicate on a new machine:

```bash
git clone --separate-git-dir=$HOME/.dotfiles git@gitlab.com:lkkmpn/dotfiles.git $HOME/dotfiles-tmp
cp ~/dotfiles-tmp ~
rm -r ~/dotfiles-tmp/
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```

dotfiles can then be added/removed by using regular `git` commands from `~`, but
with `dotfiles` instead of `git`.

This workflow is based on
[this Hacker News comment](https://news.ycombinator.com/item?id=11071754).

## Requirements

These dotfiles are obviously based on some packages:

```bash
sudo apt install i3 i3blocks blueman-applet maim playerctl powerline rclone redshift rofi scrot thefuck thunderbird xbacklight xclip xdotool
```

```bash
# install dmenu-extended
git clone https://github.com/MarkHedleyJones/dmenu-extended.git
cd dmenu-extended
sudo python setup.py install
```

```bash
# install stack-client
echo 'deb http://mirror.transip.net/stack/software/deb/Debian_9.0/ ./' | sudo tee /etc/apt/sources.list.d/stack-client.list
wget -O - https://mirror.transip.net/stack/release.key | sudo apt-key add - 
sudo apt update
sudo apt install stack-client
```

```bash
# install spotify
curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add - 
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt update
sudo apt install spotify-client
```

...and probably more that I'm forgetting.

## `scripts` directory

While the repo is called `dotfiles`, it doesn't contain just dotfiles. Deal with
it.

## Font licenses

The fonts in `.fonts` are licensed as follows:

* [Cascadia Code by Microsoft](https://github.com/microsoft/cascadia-code): SIL Open Font License 1.1
* [Source Sans Pro by Paul D. Hunt](https://github.com/adobe-fonts/source-sans-pro): SIL Open Font License 1.1
